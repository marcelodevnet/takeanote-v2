﻿using System.Collections.Generic;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.Contracts.DomainServices;

namespace TakeaNote.Application.ApplicationServices
{
    public abstract class BaseAppService<TEntity> : IBaseAppService<TEntity> where TEntity : class
    {
        private readonly IBaseDomainService<TEntity> domain;

        public BaseAppService(IBaseDomainService<TEntity> domain)
        {
            this.domain = domain;
        }

        public virtual void Insert(TEntity obj)
        {
            domain.Insert(obj);
        }

        public virtual void Update(TEntity obj)
        {
            domain.Update(obj);
        }

        public virtual void Delete(TEntity obj)
        {
            domain.Delete(obj);
        }

        public virtual TEntity FindById(int id)
        {
            return domain.FindById(id);
        }

        public virtual List<TEntity> FindAll()
        {
            return domain.FindAll();
        }

        public virtual void Dispose()
        {
            domain.Dispose();
        }
    }
}
