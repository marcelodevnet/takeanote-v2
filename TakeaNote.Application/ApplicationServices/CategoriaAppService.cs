﻿using System.Collections.Generic;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Application.ApplicationServices
{
    public class CategoriaAppService : BaseAppService<Categoria>, ICategoriaAppService
    {
        private readonly ICategoriaDomainService domain;

        public CategoriaAppService(ICategoriaDomainService domain) : base(domain)
        {
            this.domain = domain;
        }

        public void DeleteAll()
        {
            domain.DeleteAll();
        }

        public void DeletarCategoria(Categoria obj)
        {
            domain.DeletarCategoria(obj);
        }

        public void DeletarCategoriasUsuario(int id)
        {
            domain.DeletarCategoriasUsuario(id);
        }

        public int ObterTotalCategoriasPorUsuario(int id)
        {
            return domain.ObterTotalCategoriasPorUsuario(id);
        }

        public Categoria ObterCategoriaComFrases(int id)
        {
            return domain.ObterCategoriaComFrases(id);
        }

        public List<Categoria> ObterCategoriasComFrases()
        {
            return domain.ObterCategoriasComFrases();
        }

        public List<Categoria> ObterCategoriasComFrases(int pageNumber, int pageSize)
        {
            return domain.ObterCategoriasComFrases(pageNumber, pageSize);
        }

        public void AtualizarCategoriaComFrases(Categoria obj)
        {
            domain.AtualizarCategoriaComFrases(obj);
        }
    }
}
