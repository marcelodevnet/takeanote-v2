﻿using System.Collections.Generic;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.DomainEntities;
using TakeaNoteDomain.Contracts.DomainServices;

namespace TakeaNote.Application.ApplicationServices
{
    public class FraseAppService : BaseAppService<Frase>, IFraseAppService
    {
        private readonly IFraseDomainService domain;

        public FraseAppService(IFraseDomainService domain) : base(domain)
        {
            this.domain = domain;
        }

        public void DeleteAll()
        {
            domain.DeleteAll();
        }

        public void DeletarFrase(Frase obj)
        {
            domain.DeletarFrase(obj);
        }

        public void DeletarFrasesUsuario(int id)
        {
            domain.DeletarFrasesUsuario(id);
        }

        public int ObterTotalFrasesPorUsuario(int id)
        {
            return domain.ObterTotalFrasesPorUsuario(id);
        }

        public List<Frase> ObterFrasesComCategorias(int pageNumber, int pageSize)
        {
            // TODO: Dapper

            return domain.ObterFrasesComCategorias(pageNumber, pageSize);
        }

        public Frase ObterFraseComCategorias(int id)
        {
            return domain.ObterFraseComCategorias(id);
        }

        public List<Frase> ObterFrasesComCategorias()
        {
            return domain.ObterFrasesComCategorias();
        }

        public void AtualizarFraseComCategorias(Frase obj)
        {
            domain.AtualizarFraseComCategorias(obj);
        }

        
    }
}
