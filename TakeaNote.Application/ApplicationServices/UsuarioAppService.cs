﻿using TakeaNote.Application.Contracts;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Application.ApplicationServices
{
    public class UsuarioAppService : BaseAppService<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioDomainService domain;

        public UsuarioAppService(IUsuarioDomainService domain) : base(domain)
        {
            this.domain = domain;
        }

        public Usuario Autenticar(string login, string senha)
        {
            return domain.FindByLoginSenha(login, senha);
        }

        public void DeleteAll()
        {
            domain.DeleteAll();
        }

        public string ValidarTrocaSenha(string senhaTemp, string senhaAtual, string novaSenha)
        {
            return domain.ValidarTrocaSenha(senhaTemp, senhaAtual, novaSenha);
        }
    }
}
