﻿using System;
using System.Collections.Generic;

namespace TakeaNote.Application.Contracts
{
    public interface IBaseAppService<TEntity> : IDisposable where TEntity : class
    {
        void Insert(TEntity obj);
        void Update(TEntity obj);
        void Delete(TEntity obj);
        List<TEntity> FindAll();
        TEntity FindById(int id);
    }
}
