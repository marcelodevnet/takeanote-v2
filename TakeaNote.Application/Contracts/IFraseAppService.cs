﻿using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Application.Contracts
{
    public interface IFraseAppService : IBaseAppService<Frase>
    {
        void DeleteAll();
        void DeletarFrase(Frase obj);
        void DeletarFrasesUsuario(int id);

        int ObterTotalFrasesPorUsuario(int id);

        Frase ObterFraseComCategorias(int id);
        List<Frase> ObterFrasesComCategorias();

        List<Frase> ObterFrasesComCategorias(int pageNumber, int pageSize);

        void AtualizarFraseComCategorias(Frase obj);
    }
}
