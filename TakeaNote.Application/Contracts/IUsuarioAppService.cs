﻿using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Application.Contracts
{
    public interface IUsuarioAppService : IBaseAppService<Usuario>
    {
        Usuario Autenticar(string login, string senha);
        string ValidarTrocaSenha(string senhaTemp, string senhaAtual, string novaSenha);
        void DeleteAll();
    }
}
