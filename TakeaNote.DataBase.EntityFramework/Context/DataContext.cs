﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TakeaNote.DataBase.EntityFramework.Mappings;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Context
{
    public class DataContext : DbContext
    {
        public DataContext() : base(ConfigurationManager.ConnectionStrings["takeanote-v2"].ConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UsuarioMap());
            modelBuilder.Configurations.Add(new FraseMap());
            modelBuilder.Configurations.Add(new CategoriaMap());
        }

        public DbSet<Usuario> ListaUsuarios { get; set; }
        public DbSet<Frase> ListaFrases { get; set; }
        public DbSet<Categoria> ListaCategorias { get; set; }
    }
}