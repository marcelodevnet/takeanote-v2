﻿using System.Data.Entity.ModelConfiguration;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Mappings
{
    public class CategoriaMap : EntityTypeConfiguration<Categoria>
    {
        public CategoriaMap()
        {
            ToTable("Categoria");
            HasKey(c => c.IdCategoria);

            Property(c => c.IdCategoria)
                .HasColumnName("IdCategoria");

            Property(c => c.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(50)
                .IsRequired();

            Property(c => c.Descricao)
                .HasColumnName("Descricao")
                .HasMaxLength(250)
                .IsRequired();

            Property(c => c.DataCadastro)
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime")
                .IsRequired();

            Property(c => c.Visualizacao)
               .HasColumnName("Visualizacao")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();

            Property(c => c.IdUsuario)
                .HasColumnName("IdUsuario")
                .IsRequired();

            // One to Many relationship..

            HasRequired(c => c.Usuario)
                .WithMany(c => c.ListaCategorias)
                .HasForeignKey(c => c.IdUsuario);
        }
    }
}
