﻿using System.Data.Entity.ModelConfiguration;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Mappings
{
    public class FraseMap : EntityTypeConfiguration<Frase>
    {
        public FraseMap()
        {
            ToTable("Frase");
            HasKey(f => f.IdFrase);

            Property(f => f.IdFrase)
                .HasColumnName("IdFrase");

            Property(f => f.Descricao)
                .HasColumnName("Descricao")
                .HasMaxLength(250).IsRequired();

            Property(f => f.Autor)
                .HasColumnName("Autor")
                .HasMaxLength(100);

            Property(f => f.DataCadastro)
                .HasColumnName("DataCadastro")
                .HasColumnType("datetime")
                .IsRequired();

            Property(f => f.Visualizacao)
               .HasColumnName("Visualizacao")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();

            Property(f => f.IdUsuario)
                .HasColumnName("IdUsuario")
                .IsRequired();

            // One to Many relationship..

            HasRequired(f => f.Usuario)
                .WithMany(f => f.ListaFrases)
                .HasForeignKey(f => f.IdUsuario);

            // Many to Many relationship

            HasMany(x => x.ListaCategorias)
            .WithMany(x => x.ListaFrases)
            .Map(m =>
            {
                m.ToTable("FraseCategoria");
                m.MapLeftKey("IdFrase");
                m.MapRightKey("IdCategoria");
            });
        }
    }
}
