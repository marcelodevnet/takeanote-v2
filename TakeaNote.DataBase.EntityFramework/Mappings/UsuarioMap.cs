﻿using System.Data.Entity.ModelConfiguration;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Mappings
{
    public class UsuarioMap : EntityTypeConfiguration<Usuario>
    {
        public UsuarioMap()
        {
            ToTable("Usuario");
            HasKey(u => u.IdUsuario);

            Property(u => u.IdUsuario)
                .HasColumnName("IdUsuario");

            Property(u => u.Nome)
                .HasColumnName("Nome")
                .HasMaxLength(100)
                .IsRequired();

            Property(u => u.Email)
                .HasColumnName("Email")
                .HasMaxLength(50)
                .IsRequired();

            Property(u => u.Login)
               .HasColumnName("Login")
               .HasMaxLength(50)
               .IsRequired();

            Property(u => u.Senha)
               .HasColumnName("Senha")
               .HasMaxLength(50)
               .IsRequired();

            Property(u => u.DataCadastro)
               .HasColumnName("DataCadastro")
               .HasColumnType("datetime")
               .IsRequired();

            Property(u => u.Regra)
               .HasColumnName("Regra")
               .HasMaxLength(1)
               .IsFixedLength()
               .IsRequired();
        }
    }
}
