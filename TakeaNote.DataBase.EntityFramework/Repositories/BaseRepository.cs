﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Domain.Contracts.Repositories;


namespace TakeaNote.DataBase.EntityFramework.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly DataContext context;

        public bool LazyLoadingEnabled
        {
            set => context.Configuration.LazyLoadingEnabled = value;
        }

        public BaseRepository(DataContext context)
        {
            this.context = context;

            context.Database.Log = s => Debug.WriteLine(s);
        }

        public void Insert(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Added;

            context.SaveChanges();
        }

        public void InsertRange(List<TEntity> collection)
        {
            context.Set<TEntity>().AddRange(collection);

            context.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void Delete(TEntity obj)
        {
            context.Entry(obj).State = EntityState.Deleted;

            context.SaveChanges();
        }

        public void Delete(Func<TEntity, bool> predicate)
        {
            context.Set<TEntity>()
           .Where(predicate).ToList()
           .ForEach(x => context.Set<TEntity>().Remove(x));

            context.SaveChanges();
        }

        public void DeleteRange(List<TEntity> collection)
        {
            context.Set<TEntity>().RemoveRange(collection);

            context.SaveChanges();
        }

        public TEntity Find(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().FirstOrDefault(predicate);
        }

        public TEntity FindById(int id)
        {
            return context.Set<TEntity>().Find(id);
        }

        public List<TEntity> FindAll()
        {
            return context.Set<TEntity>().ToList();
        }

        public List<TEntity> FindAll(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().Where(predicate).ToList();
        }


        public int Count()
        {
            return context.Set<TEntity>().Count();
        }

        public int Count(Func<TEntity, bool> predicate)
        {
            return context.Set<TEntity>().Count(predicate);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public List<TEntity> PagedList(Func<TEntity, object> orderBy, int pageNumber, int pageSize)
        {
            return context.Set<TEntity>()
                .OrderBy(orderBy)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public List<TEntity> PagedList(Func<TEntity, bool> where, Func<TEntity, object> orderBy, int pageNumber, int pageSize)
        {
            return context.Set<TEntity>()
                .Where(where)
                .OrderBy(orderBy)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }


    }
}