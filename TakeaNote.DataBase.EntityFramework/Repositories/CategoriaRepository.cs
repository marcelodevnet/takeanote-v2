﻿using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Repositories
{
    public class CategoriaRepository : BaseRepository<Categoria>, ICategoriaRepository
    {
        private readonly DataContext context;

        public CategoriaRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
