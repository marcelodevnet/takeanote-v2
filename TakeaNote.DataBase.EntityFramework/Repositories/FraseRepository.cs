﻿using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Repositories
{
    public class FraseRepository : BaseRepository<Frase>, IFraseRepository
    {
        private readonly DataContext context;

        public FraseRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
