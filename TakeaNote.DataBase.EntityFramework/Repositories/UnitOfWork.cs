﻿using System.Data.Entity;
using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Domain.Contracts.Repositories;

namespace TakeaNote.DataBase.EntityFramework.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext context;
        private DbContextTransaction transaction;

        public UnitOfWork(DataContext context)
        {
            this.context = context;
        }

        public IFraseRepository FraseRepository => new FraseRepository(context);

        public ICategoriaRepository CategoriaRepository => new CategoriaRepository(context);

        public IUsuarioRepository UsuarioRepository => new UsuarioRepository(context);

        public void BeginTransaction()
        {
            transaction = context.Database.BeginTransaction();
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
