﻿using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.DataBase.EntityFramework.Repositories
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        private readonly DataContext context;

        public UsuarioRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
