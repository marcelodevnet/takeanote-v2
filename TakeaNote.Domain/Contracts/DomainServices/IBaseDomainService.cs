﻿using System;
using System.Collections.Generic;

namespace TakeaNote.Domain.Contracts.DomainServices
{
    public interface IBaseDomainService <TEntity> : IDisposable where TEntity : class
    {
        void Insert(TEntity obj);
        void Update(TEntity obj);
        void Delete(TEntity obj);
        List<TEntity> FindAll();
        TEntity FindById(int id);
    }
}
