﻿using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Contracts.DomainServices
{
    public interface ICategoriaDomainService : IBaseDomainService<Categoria>
    {
        void DeleteAll();
        void DeletarCategoria(Categoria obj);
        void DeletarCategoriasUsuario(int id);

        int ObterTotalCategoriasPorUsuario(int id);

        Categoria ObterCategoriaComFrases(int id);
        List<Categoria> ObterCategoriasComFrases();
        List<Categoria> ObterCategoriasComFrases(int pageNumber, int pageSize);

        void AtualizarCategoriaComFrases(Categoria obj);
    }
}
