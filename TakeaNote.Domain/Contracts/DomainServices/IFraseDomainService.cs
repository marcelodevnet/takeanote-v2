﻿using System.Collections.Generic;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNoteDomain.Contracts.DomainServices
{
    public interface IFraseDomainService : IBaseDomainService<Frase>
    {
        void DeleteAll();

        void DeletarFrase(Frase obj);

        void DeletarFrasesUsuario(int id);

        int ObterTotalFrasesPorUsuario(int id);

        Frase ObterFraseComCategorias(int id);

        List<Frase> ObterFrasesComCategorias();

        List<Frase> ObterFrasesComCategorias(int pageNumber, int pageSize);

        void AtualizarFraseComCategorias(Frase obj);
    }
}
