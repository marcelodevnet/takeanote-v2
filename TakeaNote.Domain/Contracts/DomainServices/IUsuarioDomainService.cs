﻿using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Contracts.DomainServices
{
    public interface IUsuarioDomainService : IBaseDomainService<Usuario>
    {
        Usuario FindByLoginSenha(string login, string senha);
        string ValidarTrocaSenha(string senhaTemp, string senhaAtual, string novaSenha);
        void DeleteAll();

    }
}
