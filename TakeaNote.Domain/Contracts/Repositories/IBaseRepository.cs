﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TakeaNote.Domain.Contracts.Repositories
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {
        bool LazyLoadingEnabled { set; }

        void Insert(TEntity obj);
        void InsertRange(List<TEntity> collection);
        void Update(TEntity obj);
        void Delete(TEntity obj);
        void Delete(Func<TEntity, bool> predicate);
        void DeleteRange(List<TEntity> collection);

        List<TEntity> FindAll();
        List<TEntity> FindAll(Func<TEntity, bool> predicate);

        TEntity Find(Func<TEntity, bool> predicate);
        TEntity FindById(int id);

        int Count();
        int Count(Func<TEntity, bool> predicate);

        List<TEntity> PagedList(Func<TEntity, object> orderBy, int pageNumber, int pageSize);

        List<TEntity> PagedList(Func<TEntity, bool> where, Func<TEntity, object> orderBy, int pageNumber, int pageSize);


    }
}
