﻿using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Contracts.Repositories
{
    public interface ICategoriaRepository : IBaseRepository<Categoria>
    {
    }
}
