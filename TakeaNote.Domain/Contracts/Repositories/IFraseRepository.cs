﻿using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Contracts.Repositories
{
    public interface IFraseRepository : IBaseRepository<Frase>
    {
    }
}
