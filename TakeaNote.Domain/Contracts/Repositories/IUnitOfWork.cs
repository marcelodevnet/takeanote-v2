﻿using System;

namespace TakeaNote.Domain.Contracts.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();
        void Commit();
        void Rollback();

        IFraseRepository FraseRepository { get; }
        ICategoriaRepository CategoriaRepository { get; }
        IUsuarioRepository UsuarioRepository { get; }
    }
}
