﻿using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Contracts.Repositories
{
    public interface IUsuarioRepository : IBaseRepository<Usuario>
    {
    }
}
