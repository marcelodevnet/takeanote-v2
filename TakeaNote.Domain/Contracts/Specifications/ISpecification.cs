﻿namespace TakeaNote.Domain.Contracts.Specifications
{
    public interface ISpecification<TEntity> where TEntity : class
    {
        bool IsSatisfiedBy(TEntity obj);
    }
}
