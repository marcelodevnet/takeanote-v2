﻿using System;
using System.Collections.Generic;

namespace TakeaNote.Domain.DomainEntities
{
    public class Categoria
    {
        public virtual int IdCategoria { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Descricao { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual string Visualizacao { get; set; }
        public virtual int IdUsuario { get; set; }

        // Relacionamentos

        public virtual Usuario Usuario { get; set; }
        public virtual List<Frase> ListaFrases { get; set; }

        public Categoria()
        {
            ListaFrases = new List<Frase>();
        }
    }
}
