﻿using System;
using System.Collections.Generic;

namespace TakeaNote.Domain.DomainEntities
{
    public class Frase
    {
        public virtual int IdFrase { get; set; }
        public virtual string Descricao { get; set; }
        public virtual string Autor { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual string Visualizacao { get; set; }
        public virtual int IdUsuario { get; set; }

        // Relacionamentos

        public virtual Usuario Usuario { get; set; }
        public virtual List<Categoria> ListaCategorias { get; set; }

        public Frase()
        {
            ListaCategorias = new List<Categoria>();
        }
    }
}
