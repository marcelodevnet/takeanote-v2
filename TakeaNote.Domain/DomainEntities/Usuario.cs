﻿using System;
using System.Collections.Generic;

namespace TakeaNote.Domain.DomainEntities
{
    public class Usuario
    {
        public virtual int IdUsuario { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Login { get; set; }
        public virtual string Email { get; set; }
        public virtual string Senha { get; set; }
        public virtual DateTime DataCadastro { get; set; }
        public virtual string Regra { get; set; }

        // Relacionamentos

        public virtual List<Frase> ListaFrases { get; set; }
        public virtual List<Categoria> ListaCategorias { get; set; }

        public Usuario()
        {
            ListaCategorias = new List<Categoria>();
            ListaFrases = new List<Frase>();
        }
    }
}
