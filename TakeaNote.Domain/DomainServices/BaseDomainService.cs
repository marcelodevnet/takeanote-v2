﻿using System.Collections.Generic;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.Contracts.Repositories;

namespace TakeaNote.Domain.DomainServices
{
    public class BaseDomainService<TEntity> : IBaseDomainService<TEntity> where TEntity : class
    {
        private readonly IBaseRepository<TEntity> repository;

        public BaseDomainService(IBaseRepository<TEntity> repository)
        {
            this.repository = repository;
        }

        public virtual void Insert(TEntity obj)
        {
            repository.Insert(obj);
        }

        public virtual void Update(TEntity obj)
        {
            repository.Update(obj);
        }

        public virtual void Delete(TEntity obj)
        {
            repository.Delete(obj);
        }

        public virtual List<TEntity> FindAll()
        {
            return repository.FindAll();
        }

        public virtual TEntity FindById(int id)
        {
            return repository.FindById(id);
        }

        public virtual void Dispose()
        {
            repository.Dispose();
        }
    }
}
