﻿using System;
using System.Collections.Generic;
using System.Linq;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.Domain.Specifications.Usuarios;
using TakeaNote.Util;

namespace TakeaNote.Domain.DomainServices
{
    public class CategoriaDomainService : BaseDomainService<Categoria>, ICategoriaDomainService
    {
        private readonly IUnitOfWork unitOfWork;
        public CategoriaDomainService(IUnitOfWork unitOfWork) : base(unitOfWork.CategoriaRepository)
        {
            this.unitOfWork = unitOfWork;
        }
        private Usuario ObterUsuarioLogado()
        {
            return SerializerHelper.Deserialize<Usuario>(UserHelper.SerializedUserInfo, SerializeFormat.JSON);
        }
        public void DeleteAll()
        {
            try
            {
                var collection = unitOfWork.CategoriaRepository.FindAll();

                unitOfWork.CategoriaRepository.DeleteRange(collection);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeletarCategoria(Categoria obj)
        {
            try
            {
                LimparRelacionamentos(obj);

                unitOfWork.CategoriaRepository.Delete(obj);

            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeletarCategoriasUsuario(int id)
        {
            // TODO: Dapper - DELETE FROM FRASE_CATEGORIA WHERE IDUSUARIO = X
            // TODO: Tentar fazer um unico metodo para frases e categorias

            unitOfWork.CategoriaRepository.LazyLoadingEnabled = true;

            var collection = unitOfWork.CategoriaRepository.FindAll(x => x.IdUsuario == id);

            foreach (var item in collection)
            {
                LimparRelacionamentos(item);
            }

            unitOfWork.CategoriaRepository.DeleteRange(collection);

            //TODO: PQ NAO LAZYLOADING = FALSE?
        }
        public int ObterTotalCategoriasPorUsuario(int id)
        {
            return unitOfWork.CategoriaRepository.Count(x => x.IdUsuario == id);
        }
        public Categoria ObterCategoriaComFrases(int id)
        {
            unitOfWork.CategoriaRepository.LazyLoadingEnabled = true;

            return unitOfWork.CategoriaRepository.FindById(id);
        }
        public List<Categoria> ObterCategoriasComFrases()
        {
            var usuarioSpecification = new UsuarioLogadoTemPermissoesDeAdministrador();

            if (usuarioSpecification.IsSatisfiedBy(ObterUsuarioLogado()))
            {
                return unitOfWork.CategoriaRepository.FindAll();
            }
            else
            {
                return unitOfWork.CategoriaRepository.FindAll(x => !(x.IdUsuario != ObterUsuarioLogado().IdUsuario && x.Visualizacao == "R"));
            }
        }
        public List<Categoria> ObterCategoriasComFrases(int pageNumber, int pageSize)
        {
            unitOfWork.CategoriaRepository.LazyLoadingEnabled = true;

            var usuarioSpecification = new UsuarioLogadoTemPermissoesDeAdministrador();

            if (usuarioSpecification.IsSatisfiedBy(ObterUsuarioLogado()))
            {
                return unitOfWork.CategoriaRepository.PagedList(
                    x => x.IdCategoria,
                    pageNumber,
                    pageSize);
            }
            else
            {
                return unitOfWork.CategoriaRepository.PagedList(
                    x => !(x.IdUsuario != ObterUsuarioLogado().IdUsuario && x.Visualizacao == "R"),
                    x => x.IdCategoria,
                    pageNumber,
                    pageSize);
            }
        }
        public void AtualizarCategoriaComFrases(Categoria obj)
        {
            unitOfWork.CategoriaRepository.LazyLoadingEnabled = true;

            var categoria = unitOfWork.CategoriaRepository.FindById(obj.IdCategoria);

            categoria.Nome = obj.Nome;
            categoria.Descricao = obj.Descricao;
            categoria.Visualizacao = obj.Visualizacao;

            LimparRelacionamentos(categoria);

            var listaFrasesAtualizada = new List<Frase>();

            foreach (var item in obj.ListaFrases)
            {
                var frase = unitOfWork.FraseRepository.FindById(item.IdFrase);
                listaFrasesAtualizada.Add(frase);
            }

            categoria.ListaFrases = listaFrasesAtualizada;

            unitOfWork.CategoriaRepository.Update(categoria);
        }
        private void LimparRelacionamentos(Categoria obj)
        {
            obj.ListaFrases.Clear();
            unitOfWork.CategoriaRepository.Update(obj);
        }
    }
}
