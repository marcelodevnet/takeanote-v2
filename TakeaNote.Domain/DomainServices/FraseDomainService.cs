﻿using System;
using System.Collections.Generic;
using System.Linq;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.Domain.Specifications.Usuarios;
using TakeaNote.Util;
using TakeaNoteDomain.Contracts.DomainServices;

namespace TakeaNote.Domain.DomainServices
{
    public class FraseDomainService : BaseDomainService<Frase>, IFraseDomainService
    {
        private readonly IUnitOfWork unitOfWork;
        public FraseDomainService(IUnitOfWork unitOfWork) : base(unitOfWork.FraseRepository)
        {
            this.unitOfWork = unitOfWork;
        }
        private Usuario ObterUsuarioLogado()
        {
            return SerializerHelper.Deserialize<Usuario>(UserHelper.SerializedUserInfo, SerializeFormat.JSON);
        }
        public void DeleteAll()
        {
            unitOfWork.FraseRepository.LazyLoadingEnabled = true;

            var collection = unitOfWork.FraseRepository.FindAll();

            foreach (var item in collection)
            {
                LimparRelacionamentos(item);
            }

            unitOfWork.FraseRepository.DeleteRange(collection);
        }
        public void DeletarFrase(Frase obj)
        {
            try
            {
                // TODO: Dapper
                LimparRelacionamentos(obj);

                unitOfWork.FraseRepository.Delete(obj);

            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DeletarFrasesUsuario(int id)
        {
            // TODO: Dapper - DELETE FROM FRASE_CATEGORIA WHERE IDUSUARIO = X
            // TODO: Tentar fazer um unico metodo para frases e categorias

            unitOfWork.FraseRepository.LazyLoadingEnabled = true;

            var collection = unitOfWork.FraseRepository.FindAll(x => x.IdUsuario == id);

            foreach (var item in collection)
            {
                LimparRelacionamentos(item);
            }

            unitOfWork.FraseRepository.DeleteRange(collection);
        }
        public int ObterTotalFrasesPorUsuario(int id)
        {
            return unitOfWork.FraseRepository.Count(x => x.IdUsuario == id);
        }
        public Frase ObterFraseComCategorias(int id)
        {
            // TODO: Dapper

            unitOfWork.FraseRepository.LazyLoadingEnabled = true;

            return unitOfWork.FraseRepository.FindById(id);
        }
        public List<Frase> ObterFrasesComCategorias()
        {
            var usuarioSpecification = new UsuarioLogadoTemPermissoesDeAdministrador();

            if (usuarioSpecification.IsSatisfiedBy(ObterUsuarioLogado()))
            {
                return unitOfWork.FraseRepository.FindAll();
            }
            else
            {
                return unitOfWork.FraseRepository.FindAll(x => !(x.IdUsuario != ObterUsuarioLogado().IdUsuario && x.Visualizacao == "R"));
            }

        }
        public List<Frase> ObterFrasesComCategorias(int pageNumber, int pageSize)
        {
            unitOfWork.FraseRepository.LazyLoadingEnabled = true;

            var usuarioSpecification = new UsuarioLogadoTemPermissoesDeAdministrador();

            if (usuarioSpecification.IsSatisfiedBy(ObterUsuarioLogado()))
            {
                return unitOfWork.FraseRepository.PagedList(
                    x => x.IdFrase,
                    pageNumber,
                    pageSize);
            }
            else
            {
                return unitOfWork.FraseRepository.PagedList(
                    x => !(x.IdUsuario != ObterUsuarioLogado().IdUsuario && x.Visualizacao == "R"),
                    x => x.IdFrase,
                    pageNumber,
                    pageSize);
            }
        }
        public void AtualizarFraseComCategorias(Frase obj)
        {
            unitOfWork.FraseRepository.LazyLoadingEnabled = true;

            var frase = unitOfWork.FraseRepository.FindById(obj.IdFrase);

            frase.Descricao = obj.Descricao;
            frase.Autor = obj.Autor;
            frase.Visualizacao = obj.Visualizacao;

            LimparRelacionamentos(frase);

            var listaCategoriasAtualizada = new List<Categoria>();

            foreach (var item in obj.ListaCategorias)
            {
                var categoria = unitOfWork.CategoriaRepository.FindById(item.IdCategoria);
                listaCategoriasAtualizada.Add(categoria);
            }

            frase.ListaCategorias = listaCategoriasAtualizada;

            unitOfWork.FraseRepository.Update(frase);
        }
        private void LimparRelacionamentos(Frase obj)
        {
            obj.ListaCategorias.Clear();
            unitOfWork.FraseRepository.Update(obj);
        }
    }
}
