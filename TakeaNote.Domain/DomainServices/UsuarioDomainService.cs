﻿using System;
using System.Collections.Generic;
using System.Configuration;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.Domain.Specifications.Usuarios;
using TakeaNote.Util;
using TakeaNote.Util.Resources;

namespace TakeaNote.Domain.DomainServices
{
    public class UsuarioDomainService : BaseDomainService<Usuario>, IUsuarioDomainService
    {
        private readonly IUnitOfWork unitOfWork;

        public UsuarioDomainService(IUnitOfWork unitOfWork) : base(unitOfWork.UsuarioRepository)
        {
            this.unitOfWork = unitOfWork;
        }

        public override void Insert(Usuario obj)
        {
            obj.DataCadastro = DateTime.Now;
            obj.Regra = ConfigurationManager.AppSettings["REGRA_NOVOS_USUARIOS"];

            obj.Senha = CriptorEngine.Encrypt(obj.Senha, true);

            if (HasLogin(obj.Login))
            {
                throw new Exception(Resources.CondicaoLoginEmUso);
            }

            if (HasEmail(obj.Email))
            {
                throw new Exception(Resources.CondicaoEmailEmUso);
            }

            unitOfWork.UsuarioRepository.Insert(obj);
        }

        public override Usuario FindById(int id)
        {
            var usuario = unitOfWork.UsuarioRepository.FindById(id);

            if (usuario == null)
            {
                throw new Exception(Resources.UsuarioNaoEncontrado);
            }

            usuario.Senha = CriptorEngine.Decrypt(usuario.Senha, true);

            return usuario;
        }

        public override void Update(Usuario obj)
        {

            obj.Senha = CriptorEngine.Encrypt(obj.Senha, true);

            unitOfWork.UsuarioRepository.Update(obj);
        }

        public override void Delete(Usuario obj)
        {
            var usuarioSpecification = new UsuarioExcluidoNaoEMasterNemConvidadoSpecification();

            if (usuarioSpecification.IsSatisfiedBy(obj))
            {
                unitOfWork.UsuarioRepository.Delete(obj);
            }
            else
            {
                throw new Exception(Resources.UsuarioRegraExclusaoInvalida);
            }
        }

        protected bool HasLogin(string login)
        {
            return unitOfWork.UsuarioRepository.Count(x => x.Login.Equals(login)) > 0;
        }

        protected bool HasEmail(string email)
        {
            return unitOfWork.UsuarioRepository.Count(x => x.Email.Equals(email)) > 0;
        }

        public Usuario FindByLoginSenha(string login, string senha)
        {
            var usuario = unitOfWork.UsuarioRepository
                .Find(x => x.Login.Equals(login) 
                && x.Senha.Equals(CriptorEngine.Encrypt(senha, true)));

            if (usuario == null)
            {
                throw new Exception(Resources.UsuarioSenhaInvalidos);
            }

            return usuario;
        }

        public string ValidarTrocaSenha(string senhaTemp, string senhaAtual, string novaSenha)
        {
            // Usuario altera a senha:
            if (!string.IsNullOrEmpty(senhaAtual))
            {
                if (senhaAtual == senhaTemp)
                {
                    if (!string.IsNullOrEmpty(novaSenha))
                    {
                        return novaSenha;
                    }
                    else
                    {
                        throw new Exception(Resources.CampoNovaSenhaObrigatorio);
                    }
                }
                else
                {
                    throw new Exception(Resources.CondicaoSenhaAtualNaoConfere);
                }
            }
            else
            {
                // Usuario nao altera a senha:
                return senhaTemp;
            }
        }

        public void DeleteAll()
        {
            try
            {
                var collection = unitOfWork.UsuarioRepository.FindAll();

                unitOfWork.UsuarioRepository.DeleteRange(collection);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Usuario ObterUsuarioLogado()
        {
            return SerializerHelper.Deserialize<Usuario>(UserHelper.SerializedUserInfo, SerializeFormat.JSON);
        }
    }
}
