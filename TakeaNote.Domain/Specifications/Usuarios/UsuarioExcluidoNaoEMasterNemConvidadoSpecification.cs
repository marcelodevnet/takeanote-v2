﻿using TakeaNote.Domain.Contracts.Specifications;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Specifications.Usuarios
{
    public class UsuarioExcluidoNaoEMasterNemConvidadoSpecification : ISpecification<Usuario>
    {
        public bool IsSatisfiedBy(Usuario obj)
        {
            return obj.Regra != "M" && obj.Regra != "C";
        }
    }
}
