﻿using TakeaNote.Domain.Contracts.Specifications;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.Domain.Specifications.Usuarios
{
    public class UsuarioLogadoTemPermissoesDeAdministrador : ISpecification<Usuario>
    {
        public bool IsSatisfiedBy(Usuario obj)
        {
            return obj.Regra == "M" || obj.Regra == "A";
        }
    }
}
