﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.UI.Models;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Controllers
{
    [Authorize(Roles = "M, A, U")]
    public class CategoriaController : BaseController
    {
        private readonly ICategoriaAppService categoriaAppService;
        private readonly IFraseAppService fraseAppService;

        public CategoriaController(ICategoriaAppService categoriaAppService, IFraseAppService fraseAppService)
        {
            this.categoriaAppService = categoriaAppService;
            this.fraseAppService = fraseAppService;
        }

        public ActionResult Cadastrar()
        {
            try
            {
                ViewBag.FrasesCollection = ObterFrases();

                return View();
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Cadastrar(CadastroCategoriaViewModel model)
        {
            try
            {
                var userSession = Session["USER_SESSION"] as Usuario;

                if (ModelState.IsValid)
                {
                    var frasesCollection = new List<Frase>();

                    if (model.FrasesSelecionadas != null)
                    {
                        foreach (var id in model.FrasesSelecionadas)
                        {
                            var frase = fraseAppService.FindById(int.Parse(id));
                            frasesCollection.Add(frase);
                        }
                    }

                    var categoria = new Categoria
                    {
                        Nome = model.Nome,
                        Descricao = model.Descricao,
                        DataCadastro = DateTime.Now,
                        Visualizacao = model.Visualizacao,
                        // TODO: Prevenir erro ao trazer possivel id nulo num post forçado
                        IdUsuario = userSession.IdUsuario,
                        ListaFrases = frasesCollection
                    };

                    categoriaAppService.Insert(categoria);

                    MensagensSucesso.Add(Resources.CategoriaCadastradaSucesso);

                    ModelState.Clear();
                }
                else
                {
                    MensagensErro = GetModelErrors(ModelState);
                }

                ViewBag.FrasesCollection = fraseAppService.FindAll();
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View();
        }

        [Authorize(Roles = "M, A, U")]
        public ActionResult Editar(int id)
        {
            try
            {
                var categoria = categoriaAppService.ObterCategoriaComFrases(id);

                var userSession = Session["USER_SESSION"] as Usuario;

                if (userSession.IdUsuario != categoria.IdUsuario)
                {
                    if (userSession.Regra != "M" && userSession.Regra != "A")
                    {
                        throw new Exception(Resources.CategoriaSemPermissaoEdicao);
                    }
                }

                if (categoria != null)
                {
                    // Salva o id atual para prevenir que o usuario nao edite uma categoria diferente da selecionada.

                    Session["ID_CATEGORIA_EDITADA"] = categoria.IdCategoria;

                    var model = new EdicaoCategoriaViewModel
                    {
                        IdCategoria = categoria.IdCategoria,
                        Nome = categoria.Nome,
                        Descricao = categoria.Descricao,
                        Visualizacao = categoria.Visualizacao,
                    };

                    // Recupera as categorias do bd, e associa as categorias ja atribuidas ao Usuario

                    var frasesSelecionadasCollection = new List<FrasesSelecionadasViewModel>();

                    foreach (var item in ObterFrases())
                    {
                        var x = new FrasesSelecionadasViewModel
                        {
                            IdFrase = item.IdFrase,
                            Descricao = item.Descricao
                        };

                        // verificar se a mesma pertence ao Usuario, e marca-la como checked
                        foreach (var fraseUsuario in categoria.ListaFrases)
                        {
                            if (x.IdFrase == fraseUsuario.IdFrase)
                            {
                                x.Checked = true;
                            }
                        }

                        frasesSelecionadasCollection.Add(x);
                    }

                    model.ListaFrases = frasesSelecionadasCollection;

                    return View(model);
                }
                else
                {
                    throw new Exception(Resources.CategoriaErroCarregarDados);
                }
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A, U")]
        [HttpPost]
        public ActionResult Editar(EdicaoCategoriaViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagensErro = GetModelErrors(ModelState);

                    return View(model);
                }

                var listaFrases = new List<Frase>();

                foreach (var frase in model.ListaFrases)
                {
                    if (frase.Checked)
                    {
                        var f = new Frase
                        {
                            IdFrase = frase.IdFrase
                        };

                        listaFrases.Add(f);
                    }
                }

                var categoria = new Categoria
                {
                    // TODO: Prevenir erro ao trazer possivel id nulo num post forçado
                    IdCategoria = (int)Session["ID_CATEGORIA_EDITADA"],
                    Nome = model.Nome,
                    Descricao = model.Descricao,
                    Visualizacao = model.Visualizacao,
                    ListaFrases = listaFrases
                };

                categoriaAppService.AtualizarCategoriaComFrases(categoria);

                MensagensSucesso.Add(Resources.CategoriaAtualizadaSucesso);
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View(model);
        }

        [Authorize(Roles = "M, A, U, C")]
        public ActionResult Consultar()
        {
            try
            {
                var modelCollection = new List<ConsultaCategoriaViewModel>();

                foreach (var categoria in ObterCategoriasComFrases())
                {
                    var model = new ConsultaCategoriaViewModel
                    {
                        IdCategoria = categoria.IdCategoria,
                        Nome = categoria.Nome,
                        Descricao = categoria.Descricao,
                        DataCadastro = categoria.DataCadastro,
                        Visualizacao = categoria.Visualizacao,
                        ListaFrases = categoria.ListaFrases
                    };

                    modelCollection.Add(model);
                }

                return View(modelCollection);
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A, U")]
        public ActionResult Excluir(int id)
        {
            try
            {
                var categoria = categoriaAppService.ObterCategoriaComFrases(id);

                var userSession = Session["USER_SESSION"] as Usuario;

                if (userSession.IdUsuario != categoria.IdUsuario)
                {
                    if (userSession.Regra != "M" && userSession.Regra != "A")
                    {
                        throw new Exception(Resources.CategoriaSemPermissaoExclusao);
                    }
                }

                categoriaAppService.DeletarCategoria(categoria);

                MensagensSucesso.Add(Resources.CategoriaExcluidaSucesso);

                return RedirectToAction("Consultar");
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        private List<Categoria> ObterCategoriasComFrases()
        {
            try
            {
                return categoriaAppService.ObterCategoriasComFrases();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Frase> ObterFrases()
        {
            try
            {
                return fraseAppService.ObterFrasesComCategorias();  
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}