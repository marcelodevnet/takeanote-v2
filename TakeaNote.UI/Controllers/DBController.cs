﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using TakeaNote.Domain.DTO;
using TakeaNote.Util;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Controllers
{
    [Authorize(Roles = "M, A")]
    public class DBController : BaseController
    {
        private readonly string GEARHOST_API_URL = ConfigurationManager.AppSettings["GEARHOST_API_URL"];
        private readonly string GEARHOST_API_KEY = ConfigurationManager.AppSettings["GEARHOST_API_KEY"];
        private readonly string GEARHOST_DATABASE_NAME = ConfigurationManager.AppSettings["GEARHOST_DATABASE_NAME"];

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BackupDatabase()
        {
            try
            {
                using (var webClient = new GearHostWebClient())
                {
                    webClient.Headers.Add("Authorization", string.Format("bearer {0}", GEARHOST_API_KEY));
                    
                    DatabasesDTO dto = null;

                    try
                    {
                        var databasesJson = webClient.DownloadString("https://api.gearhost.com/v1/databases");
                        dto = SerializerHelper.Deserialize<DatabasesDTO>(databasesJson, SerializeFormat.JSON);
                    }
                    catch (Exception)
                    {
                        MensagensErro.Add(Resources.ErrorDownloadingAPIDataGearHost);
                        //TODO: return false;
                    }

                    var database = dto.databases.FirstOrDefault(x => string.Compare(x.name, GEARHOST_DATABASE_NAME, StringComparison.OrdinalIgnoreCase) == 0);

                    if (database == null)
                    {
                        MensagensErro.Add(Resources.ErrorDataBaseNotFoundGearHost);
                        //TODO: return false;
                    }

                    var dataAtual = DateTime.Now;

                    string fileName = GEARHOST_DATABASE_NAME + "_" + dataAtual.ToShortDateString() + "_" + dataAtual.ToShortTimeString() + ".zip";
                    string fileType = "application/zip";
                    string fileUrl = string.Format("https://api.gearhost.com/v1/databases/{0}/backup", database.id);
  
                    byte[] fileContent = webClient.DownloadData(fileUrl);
                    
                    return File(fileContent, fileType, fileName);              
                }
            }
            catch (Exception)
            {
                MensagensErro.Add(Resources.ErroArquivoDownload);

                return View("Index");
            }
        }

        [HttpPost]
        public ActionResult RestoreDatabase()
        {
            try
            {
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    if (file.ContentLength == 0)
                    {
                        MensagensErro.Add(Resources.ErroNenhumArquivoSelecionadoOuVazio);

                        throw new ApplicationException();
                    }

                    if (Path.GetExtension(file.FileName).ToLower() != ".bak")
                    {
                        //TODO: Atualizar msg no resources
                        MensagensErro.Add(Resources.ErroFormatoArquivoJson);

                        throw new ApplicationException();
                    }

                    StreamReader reader = new StreamReader(file.InputStream);

                    string result = reader.ReadToEnd();
                    reader.Close();

                    //DataBaseDTO model = SerializerHelper.Deserialize<DataBaseDTO>(result, SerializeFormat.JSON);

                    // Antes de Restaurar, apaga os registros anteriores para evitar conflito

                    TempData["FLUXO_CONDICAO"] = "RESTORE";

                    DeleteDatabase();

                    //Retorna o fluxo, depois de deletar o BD

                    

                    FormsAuthentication.SignOut();

                    Session["USER_SESSION"] = null;

                    MensagensSucesso.Add(Resources.BDRestauradoSucesso);
                }

                return RedirectToAction("Index", "Login");
            }
            catch (ApplicationException)
            {
                return View("Index");
            }

            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return View("Index");
            }
        }

        public ActionResult DeleteDatabase()
        {
            try
            {
                var condicao = TempData["FLUXO_CONDICAO"] as string;

                // Deleta os dados, sem derrubar a sessão do usuario, retornando para função RestoreDatabase
                if (condicao == "RESTORE")
                {
                    return null;
                }

                // Se a opção na pagina escolhida for DELETE DATABASE, apaga tudo e retorna para tela de login
                else
                {
                    FormsAuthentication.SignOut();

                    Session["USER_SESSION"] = null;

                    MensagensSucesso.Add(Resources.BDApagadoSucesso);

                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception)
            {
                MensagensErro.Add(Resources.ErroDeletarBD);

                return View("Index");
            }
        }

    }
}