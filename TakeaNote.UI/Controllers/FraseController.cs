﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.UI.Models;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Controllers
{
    [Authorize(Roles = "M, A, U")]
    public class FraseController : BaseController
    {
        private readonly IFraseAppService fraseAppService;
        private readonly ICategoriaAppService categoriaAppService;
        private readonly IUsuarioAppService usuarioAppService;
        private readonly int pageSize = int.Parse(ConfigurationManager.AppSettings["MAX_ELEMENTOS_PAGINA"]);

        public FraseController(IFraseAppService fraseAppService, ICategoriaAppService categoriaAppService,
            IUsuarioAppService usuarioAppService)
        {
            this.fraseAppService = fraseAppService;
            this.categoriaAppService = categoriaAppService;
            this.usuarioAppService = usuarioAppService;
        }

        public ActionResult Cadastrar()
        {
            try
            {
                ViewBag.CategoriasCollection = ObterCategorias();

                return View();
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Cadastrar(CadastroFraseViewModel model)
        {
            try
            {
                var userSession = Session["USER_SESSION"] as Usuario;

                if (ModelState.IsValid)
                {
                    var categoriasCollection = new List<Categoria>();

                    if (model.CategoriasSelecionadas != null)
                    {
                        foreach (var id in model.CategoriasSelecionadas)
                        {
                            var categoria = categoriaAppService.FindById(int.Parse(id));
                            categoriasCollection.Add(categoria);
                        }
                    }

                    var frase = new Frase
                    {
                        Descricao = model.Descricao,
                        Autor = model.Autor,
                        Visualizacao = model.Visualizacao,
                        DataCadastro = DateTime.Now,
                        // TODO: Prevenir erro ao trazer possivel id nulo num post forçado
                        IdUsuario = userSession.IdUsuario,
                        ListaCategorias = categoriasCollection
                    };

                    fraseAppService.Insert(frase);

                    MensagensSucesso.Add(Resources.FraseCadastradaSucesso);

                    ModelState.Clear();
                }
                else
                {
                    MensagensErro = GetModelErrors(ModelState);
                }

                ViewBag.CategoriasCollection = categoriaAppService.FindAll();
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View();
        }

        [Authorize(Roles = "M, A, U")]
        public ActionResult Editar(int id)
        {
            try
            {
                var frase = fraseAppService.ObterFraseComCategorias(id);

                var userSession = Session["USER_SESSION"] as Usuario;

                if (userSession.IdUsuario != frase.IdUsuario)
                {
                    if (userSession.Regra != "M" && userSession.Regra != "A")
                    {
                        throw new Exception(Resources.FraseSemPermissaoEdicao);
                    }
                }

                if (frase != null)
                {
                    // Salva o id atual para prevenir que o usuario nao edite uma frase diferente da selecionada.

                    Session["ID_FRASE_EDITADA"] = frase.IdFrase;

                    var model = new EdicaoFraseViewModel
                    {
                        IdFrase = frase.IdFrase,
                        Descricao = frase.Descricao,
                        Autor = frase.Autor,
                        Visualizacao = frase.Visualizacao,
                    };

                    // Recupera as categorias do bd, e associa as categorias ja atribuidas ao Usuario

                    var categoriasSelecionadasCollection = new List<CategoriasSelecionadasViewModel>();

                    foreach (var item in ObterCategorias())
                    {
                        var x = new CategoriasSelecionadasViewModel
                        {
                            IdCategoria = item.IdCategoria,
                            Nome = item.Nome
                        };

                        // verificar se a mesma pertence ao Usuario, e marca-la como checked
                        foreach (var categoriaUsuario in frase.ListaCategorias)
                        {
                            if (x.IdCategoria == categoriaUsuario.IdCategoria)
                            {
                                x.Checked = true;
                            }
                        }

                        categoriasSelecionadasCollection.Add(x);
                    }

                    model.ListaCategorias = categoriasSelecionadasCollection;

                    return View(model);
                }
                else
                {
                    throw new Exception(Resources.FraseErroCarregarDados);
                }
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A, U")]
        [HttpPost]
        public ActionResult Editar(EdicaoFraseViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagensErro = GetModelErrors(ModelState);

                    return View(model);
                }

                var listaCategorias = new List<Categoria>();

                foreach (var categoria in model.ListaCategorias)
                {
                    if (categoria.Checked)
                    {
                        var c = new Categoria
                        {
                            IdCategoria = categoria.IdCategoria
                        };

                        listaCategorias.Add(c);
                    }
                }

                var frase = new Frase
                {
                    // TODO: Prevenir erro ao trazer possivel id nulo num post forçado
                    IdFrase = (int)Session["ID_FRASE_EDITADA"],
                    Descricao = model.Descricao,
                    Autor = model.Autor,
                    Visualizacao = model.Visualizacao,
                    ListaCategorias = listaCategorias
                };

                fraseAppService.AtualizarFraseComCategorias(frase);

                MensagensSucesso.Add(Resources.FraseAtualizadaSucesso);
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View(model);
        }

        [Authorize(Roles = "M, A, U, C")]
        public ActionResult Consultar(int? id)
        {
            try
            {
                int pageNumber = (id ?? 1);

                var modelCollection = new List<ConsultaFraseViewModel>();

                foreach (var frase in ObterFrasesComCategorias(pageNumber, pageSize))
                {
                    var model = new ConsultaFraseViewModel
                    {
                        IdFrase = frase.IdFrase,
                        Descricao = frase.Descricao,
                        Autor = frase.Autor,
                        Visualizacao = frase.Visualizacao,
                        DataCadastro = frase.DataCadastro,
                        ListaCategorias = frase.ListaCategorias
                    };

                    modelCollection.Add(model);
                }

                ViewBag.PageNumber = pageNumber;

                return View(modelCollection);
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A, U")]
        public ActionResult Excluir(int id)
        {
            try
            {
                var frase = fraseAppService.ObterFraseComCategorias(id);

                var userSession = Session["USER_SESSION"] as Usuario;

                if (userSession.IdUsuario != frase.IdUsuario)
                {
                    if (userSession.Regra != "M" && userSession.Regra != "A")
                    {
                        throw new Exception(Resources.FraseSemPermissaoExclusao);
                    }
                }

                fraseAppService.DeletarFrase(frase);

                MensagensSucesso.Add(Resources.FraseExcluidaSucesso);

                return RedirectToAction("Consultar");
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        private List<Frase> ObterFrasesComCategorias(int pageNumber, int pageSize)
        {
            try
            {
                ViewBag.TotalPaginas = fraseAppService.ObterFrasesComCategorias().Count() / pageSize;
                return fraseAppService.ObterFrasesComCategorias(pageNumber, pageSize);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private List<Categoria> ObterCategorias()
        {
            try
            {
                return categoriaAppService.ObterCategoriasComFrases();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}