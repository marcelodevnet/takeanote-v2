﻿using System.Web.Mvc;

namespace TakeaNote.UI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}