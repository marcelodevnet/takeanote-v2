﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.DTO;
using TakeaNote.UI.Models;
using TakeaNote.Util;
using System.Web.Mvc;

namespace TakeaNote.UI.Controllers
{
    public class LoginController : BaseController
    {
        private readonly IUsuarioAppService appService;

        public LoginController(IUsuarioAppService appService)
        {
            this.appService = appService;
        }

        public ActionResult Index()
        {
            Session["USER_SESSION"] = null;

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                var erros = new List<string>();

                if (!ModelState.IsValid)
                {
                    MensagensErro = GetModelErrors(ModelState);

                    ModelState.Clear();

                    return View("Index");
                }

                var usuario = appService.Autenticar(model.Login, model.Senha);

                FormsAuthentication.SetAuthCookie(SerializerHelper.Serialize(usuario, SerializeFormat.JSON), false);

                Session["USER_SESSION"] = usuario;

                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                ModelState.Clear();

                return View("Index");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            Session["USER_SESSION"] = null;

            Session.Abandon();

            return RedirectToAction("Index", "Login");
        }
    }
}