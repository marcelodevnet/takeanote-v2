﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.UI.Models;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Controllers
{
    public class UsuarioController : BaseController
    {
        private readonly IUsuarioAppService usuarioAppService;
        private readonly IFraseAppService fraseAppService;
        private readonly ICategoriaAppService categoriaAppService;

        public UsuarioController(IUsuarioAppService usuarioAppService, IFraseAppService fraseAppService,
            ICategoriaAppService categoriaAppService)
        {
            this.usuarioAppService = usuarioAppService;
            this.fraseAppService = fraseAppService;
            this.categoriaAppService = categoriaAppService;
        }

        public ActionResult Cadastrar()
        {
            // TODO: Exibir Dashboard
            // TODO: Remover JS desnecessários (JQuery Validade?)

            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(CadastroUsuarioViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    MensagensErro = GetModelErrors(ModelState);

                    return View();
                }

                var usuario = new Usuario
                {
                    Nome = model.Nome,
                    Login = model.Login,
                    Email = model.Email,
                    Senha = model.Senha
                };

                usuarioAppService.Insert(usuario);

                MensagensSucesso.Add(Resources.UsuarioCadastradoSucesso);

                ModelState.Clear();
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View();
        }

        [Authorize(Roles = "M, A, U")]
        public ActionResult Editar(int id)
        {
            try
            {
                var usuario = usuarioAppService.FindById(id);

                var userSession = Session["USER_SESSION"] as Usuario;

                if (userSession.IdUsuario != usuario.IdUsuario 
                    && userSession.Regra != "M" 
                    && userSession.Regra != "A")
                {
                    throw new Exception(Resources.UsuarioSemPermissaoEdicao);
                }

                if (usuario != null)
                {
                    // Exibe na pagina somente campos que podem ser alterados
                    var model = new EdicaoUsuarioViewModel
                    {
                        Nome = usuario.Nome,
                        Login = usuario.Login,
                        Email = usuario.Email,
                        Regra = usuario.Regra
                    };

                    // Salva o Usuario no tempdata para posterior comparacao
                    TempData["USER_TEMP"] = usuario;

                    return View(model);
                }
                else
                {
                    throw new Exception(Resources.UsuarioErroCarregarDados);
                }
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A, U")]
        [HttpPost]
        public ActionResult Editar(EdicaoUsuarioViewModel model)
        {
            try
            {
                var userSession = Session["USER_SESSION"] as Usuario;
                var userTemp = TempData["USER_TEMP"] as Usuario;

                if (userSession.Regra != "M" 
                    && userTemp.Login != model.Login)
                {
                    throw new Exception(Resources.CampoLoginNaoEditavel);
                }

                if (!ModelState.IsValid)
                {
                    MensagensErro = GetModelErrors(ModelState);

                    return View();
                }

                // TODO: Estudar como colocar esta validação dentro das validações na ModelState
                if (!string.IsNullOrEmpty(model.NovaSenha) 
                    && string.IsNullOrEmpty(model.SenhaAtual))
                {
                    MensagensErro.Add(Resources.CampoNovaSenhaPreenchido);
                }

                var usuario = new Usuario
                {
                    IdUsuario = userTemp.IdUsuario,
                    Nome = model.Nome,
                    Login = model.Login,
                    Email = model.Email,
                    DataCadastro = userTemp.DataCadastro,

                    Regra = string.IsNullOrEmpty(model.Regra) ? userTemp.Regra : model.Regra,
                    Senha = usuarioAppService.ValidarTrocaSenha(userTemp.Senha, model.SenhaAtual, model.NovaSenha)
                };

                usuarioAppService.Update(usuario);

                MensagensSucesso.Add(Resources.UsuarioAtualizadoSucesso);
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);
            }

            return View();
        }

        [Authorize(Roles = "M, A")]
        public ActionResult Consultar()
        {
            try
            {
                return View(CarregarUsuarios());
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        [Authorize(Roles = "M, A")]
        public ActionResult Excluir(int id)
        {
            try
            {
                fraseAppService.DeletarFrasesUsuario(id);
                categoriaAppService.DeletarCategoriasUsuario(id);

                var usuario = usuarioAppService.FindById(id);

                usuarioAppService.Delete(usuario);

                MensagensSucesso.Add(Resources.UsuarioExcluidoSucesso);

                return RedirectToAction("Consultar");
            }
            catch (Exception ex)
            {
                MensagensErro.Add(ex.Message);

                return RedirectToAction("Index", "Home");
            }
        }

        private List<ConsultaUsuarioViewModel> CarregarUsuarios()
        {
            try
            {
                var listaModel = new List<ConsultaUsuarioViewModel>();

                foreach (var usuario in usuarioAppService.FindAll())
                {
                    var model = new ConsultaUsuarioViewModel
                    {
                        IdUsuario = usuario.IdUsuario,
                        Nome = usuario.Nome,
                        Login = usuario.Login,
                        Email = usuario.Email,
                        Senha = usuario.Senha,
                        DataCadastro = usuario.DataCadastro,
                        Regra = usuario.Regra,
                        TotalFrases = fraseAppService.ObterTotalFrasesPorUsuario(usuario.IdUsuario),
                        TotalCategorias = categoriaAppService.ObterTotalCategoriasPorUsuario(usuario.IdUsuario)
                    };

                    listaModel.Add(model);
                }

                return listaModel;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}