﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using TakeaNote.Application.ApplicationServices;
using TakeaNote.Application.Contracts;
using TakeaNote.Domain.Contracts.DomainServices;
using TakeaNote.Domain.Contracts.Repositories;
using TakeaNote.Domain.DomainServices;
using TakeaNoteDomain.Contracts.DomainServices;
using Newtonsoft.Json;
using TakeaNote.Domain.DomainEntities;
using TakeaNote.DataBase.EntityFramework.Repositories;
using TakeaNote.DataBase.EntityFramework.Context;
using TakeaNote.Util;

namespace TakeaNote.UI
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SimpleInjectorConfig();
            JsonSerializeConfig();
        }

        private void SimpleInjectorConfig()
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<IFraseRepository, FraseRepository>(Lifestyle.Scoped);
            container.Register<ICategoriaRepository, CategoriaRepository>(Lifestyle.Scoped);
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);

            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<DataContext>(Lifestyle.Scoped);

            container.Register<IFraseDomainService, FraseDomainService>(Lifestyle.Scoped);
            container.Register<ICategoriaDomainService, CategoriaDomainService>(Lifestyle.Scoped);
            container.Register<IUsuarioDomainService, UsuarioDomainService>(Lifestyle.Scoped);

            container.Register<IFraseAppService, FraseAppService>(Lifestyle.Scoped);
            container.Register<ICategoriaAppService, CategoriaAppService>(Lifestyle.Scoped);
            container.Register<IUsuarioAppService, UsuarioAppService>(Lifestyle.Scoped);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity is FormsIdentity id)
                    {
                        FormsAuthenticationTicket ticket = id.Ticket;

                        var usuario = SerializerHelper.Deserialize<Usuario>(ticket.Name, SerializeFormat.JSON);
                        string[] role = { usuario.Regra };

                        HttpContext.Current.User = new GenericPrincipal(id, role);

                        // Salva o usuario logado atual, para acesso nos projetos class library
                        UserHelper.SerializedUserInfo = ticket.Name;
                    }
                }
            }
        }

        private void JsonSerializeConfig()
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }
    }
}
