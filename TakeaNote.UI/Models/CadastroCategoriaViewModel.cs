﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    public class CadastroCategoriaViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoNomeObrigatorio")]
        [Display(Name = "Nome da Nova Categoria")]
        public string Nome { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoDescricaoObrigatorio")]
        [Display(Name = "Descrição da Nova Categoria")]
        public string Descricao { get; set; }

        public DateTime DataCadastro { get; set; }

        [Display(Name = "Visualização")]
        public string Visualizacao { get; set; }

        public IList<string> FrasesSelecionadas { get; set; }
    }
}