﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    public class CadastroFraseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoDescricaoObrigatorio")]
        [Display(Name = "Descrição da Nova Frase")]
        public string Descricao { get; set; }

        [Display(Name = "Autor ou Referência")]
        public string Autor { get; set; }

        [Display(Name = "Visualização")]
        public string Visualizacao { get; set; }

        public IList<string> CategoriasSelecionadas { get; set; }
    }
}