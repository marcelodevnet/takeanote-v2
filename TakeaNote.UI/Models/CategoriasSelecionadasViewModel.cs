﻿namespace TakeaNote.UI.Models
{
    public class CategoriasSelecionadasViewModel
    {
        public int IdCategoria { get; set; }
        public string Nome { get; set; }
        public bool Checked { get; set; }
    }
}