﻿using System;
using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.UI.Models
{
    public class ConsultaCategoriaViewModel
    {
        public int IdCategoria { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Visualizacao { get; set; }
        public List<Frase> ListaFrases { get; set; }
    }
}