﻿using System;
using System.Collections.Generic;
using TakeaNote.Domain.DomainEntities;

namespace TakeaNote.UI.Models
{
    public class ConsultaFraseViewModel
    {
        public int IdFrase { get; set; }
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Visualizacao { get; set; }
        public List<Categoria> ListaCategorias { get; set; }
    }
}