﻿using System;

namespace TakeaNote.UI.Models
{
    public class ConsultaUsuarioViewModel
    {
        public int IdUsuario { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Regra { get; set; }
      
        public int TotalFrases { get; set; }
        public int TotalCategorias { get; set; }
    }
}