﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeaNote.Domain.DTO;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    // TODO: Trocar todas as "Listas" por "ObjetoCollection"

    public class EdicaoCategoriaViewModel
    {
        public int IdCategoria { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoNomeObrigatorio")]
        [Display(Name = "Nome da Categoria")]
        public string Nome { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoDescricaoObrigatorio")]
        [Display(Name = "Descrição da Categoria")]
        public string Descricao { get; set; }

        [Display(Name = "Visualização")]
        public string Visualizacao { get; set; }

        public List<FrasesSelecionadasViewModel> ListaFrases { get; set; }

        public EdicaoCategoriaViewModel()
        {
            ListaFrases = new List<FrasesSelecionadasViewModel>();
        }
    }
}