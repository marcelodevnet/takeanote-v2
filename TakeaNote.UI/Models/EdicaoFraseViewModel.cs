﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    public class EdicaoFraseViewModel
    {
        public int  IdFrase { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoDescricaoObrigatorio")]
        [Display(Name = "Descrição da Frase")]
        public string Descricao { get; set; }

        [Display(Name = "Autor ou Referência")]
        public string Autor { get; set; }

        [Display(Name = "Visualização")]
        public string Visualizacao { get; set; }

        public List<CategoriasSelecionadasViewModel> ListaCategorias { get; set; }

        public EdicaoFraseViewModel()
        {
            ListaCategorias = new List<CategoriasSelecionadasViewModel>();
        }
    }
}