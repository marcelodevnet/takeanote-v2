﻿using System.ComponentModel.DataAnnotations;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    public class EdicaoUsuarioViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoNomeObrigatorio")]
        public string Nome { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoLoginObrigatorio")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoEmailObrigatorio")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoEmailInvalido")]
        public string Email { get; set; }

        public string Regra { get; set; }

        [Display(Name = "Senha Atual")]
        public string SenhaAtual { get; set; }

        [Display(Name = "Nova Senha")]
        public string NovaSenha { get; set; }
    }
}