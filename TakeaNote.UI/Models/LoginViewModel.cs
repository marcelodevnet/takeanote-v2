﻿using System.ComponentModel.DataAnnotations;
using TakeaNote.Util.Resources;

namespace TakeaNote.UI.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoLoginObrigatorio")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "CampoSenhaObrigatorio")]
        public string Senha { get; set; }
    }
}