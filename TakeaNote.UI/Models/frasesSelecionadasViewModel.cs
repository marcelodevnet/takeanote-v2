﻿namespace TakeaNote.UI.Models
{
    public class FrasesSelecionadasViewModel
    {
        public int IdFrase { get; set; }
        public string Descricao { get; set; }
        public bool Checked { get; set; }
    }
}